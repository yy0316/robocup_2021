#ifndef _UART_HMI_H_
#define _UART_HMI_H_


#include "string.h"
#include "cmd.h"
#include "usart.h"

extern char HMI_LeftCode_Start[9]; //文本1开始字符串
extern char HMI_RightCode_Start[9]; //文本2开始字符串 

extern char HMI_Code_End[2];  //文本结束字符串 "

extern char HMI_LeftCode[22];  //串口向串口屏发送这个数组 显示条码1
extern char HMI_RightCode[22];  //串口向串口屏发送这个数组 显示条码2
//char HMI_LeftCode[23];  //串口向串口屏发送这个数组 显示条码1
//char HMI_RightCode[23];  //串口向串口屏发送这个数组 显示条码2
extern uint8_t Code_Number[13];  //文本中间字符串  扫码模块发送的13位条码数值 

extern char HMI_PlayAudio[10]; //播放选择的音频
extern char HMI_ChooseAudio_Left[11]; //选择音频0: “1床病人请取药”
extern char HMI_ChooseAudio_Right[11]; //选择音频1: “3床病人请取药”

extern char HMI_End[3]; //串口屏命令结束符

void HMI_ShowCode_Left(UART_HandleTypeDef *huart);
void HMI_ShowCode_Right(UART_HandleTypeDef *huart);
void HMI_PlayAudio_Left(UART_HandleTypeDef *huart);
void HMI_PlayAudio_Right(UART_HandleTypeDef *huart);
void Code_Scan(void);


#endif


