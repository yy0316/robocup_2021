/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
extern char TimeFlag_5ms;
extern char TimeFlag_20ms;
extern char TimeFlag_1s;
extern char TimeFlag_100ms;
extern unsigned int TimeFlag_10ms;
extern char TimeFlag_50ms;
typedef struct{

  int track_right_flag;
  int track_left_flag;
  int track_memory ; 
  int first_flag ; 
  int bed2bed_flag;
  int track_cnt_flag;
  int first_intrack ; 
  int begin_count ; 
  int laser_update_pos ; 
  int voice_flag ;
  int last_flag ; 
  int right_voice;
  int left_voice ; 
  int laser_track; 

}Flag;
typedef struct{
  int drug_left_put ; 
  int drug_right_put ; 
  int drug_left_close ; 
  int drug_right_close ; 
  int servo_left ; 
  int servo_right ; 
  int servo_reset ; 
  int count_servo ;
  int servo_finished ; 
  int motor_back ; 
  int motor_front ;
}Servo_flag;
extern Flag flag;
extern Servo_flag servo_flag ; 
extern int go_to_point_flag ;
extern int xxx ; 
extern int yyy ; 
extern int zzz ;
void inc(void);

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define Triger1_Pin GPIO_PIN_4
#define Triger1_GPIO_Port GPIOF
#define HallSwitch1_Pin GPIO_PIN_5
#define HallSwitch1_GPIO_Port GPIOF
#define HallSwitch2_Pin GPIO_PIN_6
#define HallSwitch2_GPIO_Port GPIOF
#define HallSwitch3_Pin GPIO_PIN_7
#define HallSwitch3_GPIO_Port GPIOF
#define HallSwitch4_Pin GPIO_PIN_8
#define HallSwitch4_GPIO_Port GPIOF
#define Left_Right_Pin GPIO_PIN_6
#define Left_Right_GPIO_Port GPIOD
#define Relay_Pin GPIO_PIN_4
#define Relay_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
