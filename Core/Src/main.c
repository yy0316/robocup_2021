/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "can.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include "simplelib.h"
#include "cmd_func.h"

#include "vesc_can.h"
#include "rudder_chassis.h"
#include "dji_board_can.h"
#include "move_test.h"
#include "base_chassis.h"
#include "omni_chassis.h"
#include <math.h>
#include "laser.h"
#include "uart_hmi.h"
// extern int motor_speed_ctr;

//2021-11-29 21:47
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
extern Flag flag = {
    //    int track_right_flag;
    //  int track_left_flag;
    //  int servo_down_flag;
    //  int servo_up_left_flag;
    //  int servo_up_right_flag;

    0,
    0,
    0, //记录�?始跑的轨迹，如果是右边则�?1，如果左边则�?2，以此判断从�?个床到另�?个床的方�?
    0,
    0,
    0, //记录了跑了几段轨�?
    1, // first_intrack
    1, // begin_count
    0, // laser_update_pos
    0, // voice_flag
    0, // last
    0,
    0,
    1,

};
extern Servo_flag servo_flag = {
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    1,
    0,
    0,
    1,
};
extern int xxx = 0;
extern int yyy = 0;
extern int zzz = 0;
int exe_flag = 0;
float MOTOR_3WHEEL_SPEED =0;
float MOTOR_3WHEEL_DIRECTION = 0;
float MOTOR_3WHEEL_OMEGA = 0;
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_CAN1_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  MX_ADC1_Init();
  MX_ADC2_Init();
  MX_ADC3_Init();
  MX_TIM4_Init();
  MX_USART3_UART_Init();
  MX_CAN2_Init();
  /* USER CODE BEGIN 2 */

  SimpleLib_Init(&huart1, &hcan1);
  VelLoopCfg(CAN1, 1, 10, 10);
  VelLoopCfg(CAN1, 2, 10, 10);
  VelLoopCfg(CAN1, 3, 10, 10);
  PosLoopCfg(CAN1, 4, 10, 10, 500);
  MotorOn(CAN1, 1);
  MotorOn(CAN1, 2);
  MotorOn(CAN1, 3);
  MotorOn(CAN1, 4);
  
  //  int cnt = 1;
  OmniChassis_Init();
  OmniChassis.base->ctrl_mode = CTRL_MODE_TRACK;
  //OmniChassis.base->ctrl_mode = CTRL_MODE_CMD ; 
  OmniChassis.base->pos_mode = POS_MODE_RELATIVE;
  // OmniChassis.base->TrackStatus.track_path_index = 1;
  // OmniChassis.base->TrackStatus.start = 1;
  // OmniChassis.base->TrackStatus.finished = 1;
  Chassis_RemappingPathSetsSpeed(points_pos1, 31, 1.0f / 888.0f, 0, 0.05, 0.25);//0.45
  Chassis_RemappingPathSetsSpeed(points_pos2, 31, 1.0f / 888.0f, 0, 0.05, 0.15);//0.4
  Chassis_RemappingPathSetsSpeed(points_pos3, 31, 1.0f / 888.0f, 0, 0.05, 0.25);//0.45
  Chassis_RemappingPathSetsSpeed(points_pos4, 31, 1.0f / 888.0f, 0, 0.05, 0.25);//0.45
  Chassis_RemappingPathSetsSpeed(points_pos5, 31, 1.0f / 1500.0f, 0, 0.05, 0.15);//0.4
  Chassis_RemappingPathSetsSpeed(points_pos6, 31, 1.0f / 888.0f, 0, 0.05, 0.25);//0.45
  laser_init();
  int iflag = 1;
  if(HAL_GPIO_ReadPin(Left_Right_GPIO_Port,Left_Right_Pin) == GPIO_PIN_SET)
  {
    flag.track_left_flag = 1;
  }
  else{
    flag.track_right_flag = 1;
  }
 // flag.track_left_flag = 1;
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    SimpleLib_Run();

    if (exe_flag == 1)
    {
//     Open_Right_box();
      
//     Close_Right_box();
   //  Open_Left_box();
     //Close_Left_box();
   //  / HMI_PlayAudio_Right(&huart2);
     // HMI_PlayAudio_Left(&huart2);
   OmniChassis_Exe();
   
//   Open_Left_box();
//   Close_Left_box();
//     
  // Choose_Left_box();
  // ERROR_Choose_Left_box();
    // Reset_box_position();
    //  Choose_Right_box();
    //  Reset_box_position();
     
   //Motor_front();
  //  Motor_back();
      laser_exe();
    // laser_print_distance();
//laser_print_adc();
      uprintf("x: %7.4f y: %7.4f \r\n", OmniChassis.base->PostureStatus.x, OmniChassis.base->PostureStatus.y);
         exe_flag = 0;
    }

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
static unsigned int TimeFlag_1ms = 0;
unsigned int TimeFlag_10ms = 0;

// 以下定时标志位翻转周�?10ms
char TimeFlag_5ms = 0;
char TimeFlag_1s = 0;
char TimeFlag_20ms = 0;
char TimeFlag_100ms = 0;
char TimeFlag_50ms = 0;
/**
 * @brief 分频定时�?
 */
void inc(void)
{
  TimeFlag_1ms++;
  xxx++;
  yyy++;
  zzz++;
  if (TimeFlag_1ms % 500 == 0) // 500ms
  {
    
  }

  if (TimeFlag_1ms % 100 == 0) // 100ms
  {
  }

  if (TimeFlag_1ms % 50 == 0) // 50ms=20hz
  {
    // RudderChassis.SteerMotors.can_send_flag = 1;
    // RudderChassis.DriveMotors.can_send_flag = 1;
    
  }
  // 10ms
  if (TimeFlag_1ms % 10 == 0)
  {
    TimeFlag_20ms = (TimeFlag_10ms % 2 == 0) ? 1 : 0;
    TimeFlag_50ms = (TimeFlag_10ms % 5 == 0) ? 1 : 0;
    TimeFlag_100ms = (TimeFlag_10ms % 10 == 0) ? 1 : 0;
    TimeFlag_1s = (TimeFlag_10ms % 100 == 0) ? 1 : 0;
    TimeFlag_10ms++;
   // exe_flag = 1;
    exe_flag = 1;
  }

  // 5ms
  if (TimeFlag_1ms % 5 == 0)
  {
    TimeFlag_5ms = 1;

    //   uprintf("%f %f x:%f y:%f\r\n",OmniChassis.base->target_speed ,OmniChassis.base->target_dir ,OmniChassis.base->PostureStatus.x,OmniChassis.base->PostureStatus.y);
    // uprintf("%f %f\r\n",OmniChassis.base->PostureStatus.x,OmniChassis.base->PostureStatus.y);
    // flag.lcd_flag = 1;
    // flag.chassis_laser_flag = 1;

    //        if (chassis_status_tx.vega_is_ready == 1)
    //        {
    //          flag.chassis_control_flag = 1;
    //        }
    // uprintf("%f %f %f\r\n",Mode2_pos_x,Mode2_pos_y,Mode2_pos_angle);
  }
  else
  {
    TimeFlag_5ms = 0;
  }
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
