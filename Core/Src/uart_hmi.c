#include "uart_hmi.h"
#include <string.h>
#include "main.h"
char HMI_LeftCode_Start[9]="t1.txt=\""; //文本1开始字符串
char HMI_RightCode_Start[9]="t2.txt=\""; //文本2开始字符串 

char HMI_Code_End[2]="\"";  //文本结束字符串 "

char HMI_LeftCode[22];  //串口向串口屏发送这个数组 显示条码1
char HMI_RightCode[22];  //串口向串口屏发送这个数组 显示条码2

extern uint8_t Code_Number[13];  //文本中间字符串  扫码模块发送的13位条码数值 

char HMI_PlayAudio[10]="wav0.en=1"; //播放选择的音频
char HMI_ChooseAudio_Left[11]="wav0.vid=0"; //选择音频0: “1床病人请取药”
char HMI_ChooseAudio_Right[11]="wav0.vid=1"; //选择音频1: “3床病人请取药”

char HMI_End[3]={0xff,0xff,0xff}; //串口屏命令结束符

/**
 * @brief 显示左床条码1的数值
 * 
 * @param huart 
 */
void HMI_ShowCode_Left(UART_HandleTypeDef *huart)
{
    //合成包含条形码的字符串
    for(int i=0;i<22;i++)
    {
       if(i<8) HMI_LeftCode[i]=HMI_LeftCode_Start[i];
       else if((7<i)&&(i<21)) HMI_LeftCode[i]=Code_Number[i-8];
       //else if(i==21) HMI_LeftCode[i]=HMI_Code_End[0];
       HMI_LeftCode[21]='\"';
    }
    //发送显示左边1床条码的字符串
    HAL_UART_Transmit(huart,(uint8_t*)HMI_LeftCode,22,10);
    //发送字符串结束符
    HAL_UART_Transmit(huart,(uint8_t*)HMI_End,3,HAL_MAX_DELAY);
}

/**
 * @brief 显示右床条码2的数值
 * 
 * @param huart 
 */
void HMI_ShowCode_Right(UART_HandleTypeDef *huart)
{
    //合成包含条形码的字符串
    for(int i=0;i<22;i++)
    {
       if(i<8) HMI_RightCode[i]=HMI_RightCode_Start[i];
       else if((7<i)&&(i<21)) HMI_RightCode[i]=Code_Number[i-8];
       //else if(i==21) HMI_RightCode[i]=HMI_Code_End[0];
       HMI_RightCode[21]='\"';
    }
    //发送显示右边3床条码的字符串
    HAL_UART_Transmit(huart,(uint8_t*)HMI_RightCode,22,10);
    //发送字符串结束符
    HAL_UART_Transmit(huart,(uint8_t*)HMI_End,3,HAL_MAX_DELAY);
    
}

/**
 * @brief 播放左床音频0:  “1床病人请取药”
 * 
 * @param huart 
 */
void HMI_PlayAudio_Left(UART_HandleTypeDef *huart)
{
    //选择左边的音频0
    HAL_UART_Transmit(huart,(uint8_t*)HMI_ChooseAudio_Left,strlen(HMI_ChooseAudio_Left),HAL_MAX_DELAY);
    //发送字符串结束符
    HAL_UART_Transmit(huart,(uint8_t*)HMI_End,3,HAL_MAX_DELAY);
    //播放选定的音频
    HAL_UART_Transmit(huart,(uint8_t*)HMI_PlayAudio,strlen(HMI_PlayAudio),HAL_MAX_DELAY);
    //发送字符串结束符
    HAL_UART_Transmit(huart,(uint8_t*)HMI_End,3,HAL_MAX_DELAY);
   
}

/**
 * @brief 播放右床音频1:  “3床病人请取药” 
 * 
 * @param huart 
 */
void HMI_PlayAudio_Right(UART_HandleTypeDef *huart)
{
    //选择右边的音频1
    HAL_UART_Transmit(huart,(uint8_t*)HMI_ChooseAudio_Right,strlen(HMI_ChooseAudio_Right),HAL_MAX_DELAY);
    //发送字符串结束符
    HAL_UART_Transmit(huart,(uint8_t*)HMI_End,3,HAL_MAX_DELAY);
    //播放选定的音频
    HAL_UART_Transmit(huart,(uint8_t*)HMI_PlayAudio,strlen(HMI_PlayAudio),HAL_MAX_DELAY);
    //发送字符串结束符
    HAL_UART_Transmit(huart,(uint8_t*)HMI_End,3,HAL_MAX_DELAY);   
}
void Code_Scan(void)
{
    for(int i=0;i<10;i++)
    {
        HAL_GPIO_WritePin(Triger1_GPIO_Port,Triger1_Pin,GPIO_PIN_RESET);//0
        HAL_Delay(50);  //延时50ms   此处需要改成对应的延时
        HAL_GPIO_WritePin(Triger1_GPIO_Port,Triger1_Pin,GPIO_PIN_SET); //1
        HAL_Delay(50);  //延时50ms   此处需要改成对应的延时
    }
}

