#ifndef MOVE_TEST_H_
#define MOVE_TEST_H_
#include "dji_board_can.h"
#include "stm32f407xx.h"

void MOVE_TEST_FRONT(CAN_TypeDef *CANx, int32_t vel);
void MOVE_TEST_AROUND(CAN_TypeDef *CANx, int32_t vel);
void Omni_ChassisMove_3Wheel(float speed,float direction,float target_angle);
void Choose_Left_box(void);
void Choose_Right_box(void);
void Reset_box_position(void);
void Open_Left_box(void);
void Close_Left_box(void);
void Open_Right_box(void);
void Close_Right_box(void);
void Motor_front();
void Motor_back();
void Servo_Exe();

#endif 