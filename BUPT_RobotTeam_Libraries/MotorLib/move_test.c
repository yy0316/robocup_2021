#include "dji_board_can.h"
#include "move_test.h"
#include "main.h"
#include "stm32f407xx.h"
#include "base_chassis.h"
#include "omni_chassis.h"
#include "chassis_common_config.h"
#include "can_utils.h"
#include "utils.h"
#include "handle.h"
#include <math.h>
#include "dji_board_can.h"
#include "cmd.h"
#include "vec.h"
#include "points.h"
#include "base_chassis.h"
#include "points.h"
#include "move.h"
#include "tim.h"
#include "uart_hmi.h"
//��������
#define CHASSIS_LENGTH (0.438)
#define MOTOR_RADIUS (0.0218)
#define WHEEL_RADIUS (0.075)

#define  USE_CHASSIS_OMNI_3WHEEL
#ifdef USE_CHASSIS_OMNI_3WHEEL
//OmniChassis_t OmniChassis;

static float InstallAngle_3Wheel[3] = {-1.04719755, 1.04719755, 0}; // �������������������ϵx��ļн�/rad
PID_Struct y_pid = {3000, 125000, 0, 0, 0, 5000, 0, 0.005}; //�ٶȷ������

PID_Struct angle_pid = {1000, 0, 0, 0, 0, 5000, 0, 0.005}; //ƫ�߽ǿ���
float Arrive_distance = 0.05;
//char MotorCANSendFlag = 0; // ��Ҫ�ڶ�ʱ�ж�����1�����Ʒ���Ƶ��





// void Send_drug()
// {
//   DJI_PosCtrl(CAN2 , 1 , ABSOLUTE_MODE , 8096 * 36 * 4); 
// }

void Choose_Left_box(void)
{

  float Angle = 10;
  float Compare = 25 + (Angle*100)/180;
  HAL_TIM_PWM_Start(&htim4,TIM_CHANNEL_1);
  __HAL_TIM_SET_COMPARE(&htim4,TIM_CHANNEL_1,(uint32_t)Compare);

}

void Choose_Right_box(void)
{

  float Angle = 165;
  float Compare = 35 + (Angle*100)/180;
  HAL_TIM_PWM_Start(&htim4,TIM_CHANNEL_1);
  __HAL_TIM_SET_COMPARE(&htim4,TIM_CHANNEL_1,(uint32_t)Compare);

}

void Reset_box_position(void)
{

  float Angle = 90;
  float Compare = 25 + (Angle*100)/180;
  HAL_TIM_PWM_Start(&htim4,TIM_CHANNEL_1);
  __HAL_TIM_SET_COMPARE(&htim4,TIM_CHANNEL_1,(uint32_t)Compare);

}

void Open_Left_box(void)
{
  float Angle = 25;
  float Compare = 25 + (Angle*100)/180;
  HAL_TIM_PWM_Start(&htim4,TIM_CHANNEL_2);
  __HAL_TIM_SET_COMPARE(&htim4,TIM_CHANNEL_2,(uint32_t)Compare);

}

void Close_Left_box(void)
{
  float Angle = 70;
  float Compare = 25 + (Angle*100)/180;
  HAL_TIM_PWM_Start(&htim4,TIM_CHANNEL_2);
  __HAL_TIM_SET_COMPARE(&htim4,TIM_CHANNEL_2,(uint32_t)Compare);

}
void ERROR_Choose_Left_box(void)
{
  float Angle = 20;
  float Compare = 25 + (Angle*100)/180;
  HAL_TIM_PWM_Start(&htim4,TIM_CHANNEL_1);
  __HAL_TIM_SET_COMPARE(&htim4,TIM_CHANNEL_1,(uint32_t)Compare);
}
void Open_Right_box(void)
{
  float Angle = 0;
  float Compare = 25 + (Angle*100)/180;
  HAL_TIM_PWM_Start(&htim4,TIM_CHANNEL_3);
  __HAL_TIM_SET_COMPARE(&htim4,TIM_CHANNEL_3,(uint32_t)Compare);

}

void Close_Right_box(void)
{
  float Angle = 70;
  float Compare = 25 + (Angle*100)/180;
  HAL_TIM_PWM_Start(&htim4,TIM_CHANNEL_3);
  __HAL_TIM_SET_COMPARE(&htim4,TIM_CHANNEL_3,(uint32_t)Compare);

}
void Motor_front()
{
  DJI_PosCtrl(CAN1, 4, ABSOLUTE_MODE , -(8192*17*3) );
  yyy = 0 ; 
  for(;yyy<1000;)
  {}
  DJI_PosCtrl(CAN1, 4, ABSOLUTE_MODE , -(8192*17*3) );
  
}
void Motor_back()
{
  //DJI_PosCtrl(CAN1, 4, 0 , 8192*2 );
}
void Motor_back_real()
{
  DJI_PosCtrl(CAN1, 4, 0 , 8192*2 );
}
void Servo_Exe()
{
	if(servo_flag.motor_front == 1)
	{
		if(servo_flag.count_servo == 1)
		{
			Motor_front();
			yyy = 0;
			servo_flag.count_servo = 0;
			//flag.voice_flag = 1;
		}
		
		if(yyy>0)
		{
			servo_flag.motor_front = 0; 
			servo_flag.count_servo = 1;
		}
		
		
	}
	else if(servo_flag.motor_back == 1)
	{
		if(servo_flag.count_servo == 1)
		{
			Motor_back();
			yyy = 0 ;
			servo_flag.count_servo = 0;                
                        servo_flag.motor_back = 0 ; 
                        servo_flag.count_servo = 1;
                        servo_flag.servo_finished = 1 ; 
                }
	}
	else if(servo_flag.servo_finished == 0 && servo_flag.motor_back!=1 &&servo_flag.motor_front!=1)
	{
	if(servo_flag.servo_left == 1 )
	{
		
		if (servo_flag.count_servo == 1)
		{
                  if (flag.first_flag == 1 && flag.track_memory == 1&&flag.bed2bed_flag == 0){ERROR_Choose_Left_box();}
                  else{
			Choose_Left_box();
                  }
			yyy = 0 ; 
			servo_flag.count_servo = 0;
		}
		if(yyy > 0)
		{
			servo_flag.servo_left = 0 ; 
			servo_flag.count_servo = 1 ; 
			servo_flag.drug_left_put  = 1;
		}
	}
	if(servo_flag.servo_right == 1 )
	{
		
		if (servo_flag.count_servo == 1)
		{
                  
			Choose_Right_box();
			yyy = 0 ; 
			servo_flag.count_servo = 0;
		}
		if(yyy > 0)
		{
			servo_flag.servo_right = 0 ; 
			servo_flag.count_servo = 1 ; 
			servo_flag.drug_right_put  = 1;
		}
	}
	if(servo_flag.drug_left_put == 1 )
	{
		
		if (servo_flag.count_servo == 1)
		{
			Open_Left_box();
			yyy = 0 ; 
			servo_flag.count_servo = 0;
		}
		if(yyy > 0)
		{
			servo_flag.drug_left_put = 0 ; 
			servo_flag.count_servo = 1 ; 
			servo_flag.drug_left_close  = 1;
		}
	}
	if(servo_flag.drug_right_put == 1 )
	{
		
		if (servo_flag.count_servo == 1)
		{
			Open_Right_box();
			yyy = 0 ; 
			servo_flag.count_servo = 0;
		}
		if(yyy > 0)
		{
			servo_flag.drug_right_put = 0 ; 
			servo_flag.count_servo = 1 ; 
			servo_flag.drug_right_close  = 1;
		}
	}
	if(servo_flag.drug_left_close == 1 )
	{
		
		if (servo_flag.count_servo == 1)
		{
			Close_Left_box();
			yyy = 0 ; 
			servo_flag.count_servo = 0;
		}
		if(yyy > 0)
		{
			servo_flag.drug_left_close = 0 ; 
			servo_flag.count_servo = 1 ; 
			servo_flag.servo_reset  = 1;
		}
	}
	if(servo_flag.drug_right_close == 1 )
	{
		
		if (servo_flag.count_servo == 1)
		{
			Close_Right_box();
			yyy = 0 ; 
			servo_flag.count_servo = 0;
		}
		if(yyy > 0)
		{
			servo_flag.drug_right_close = 0 ; 
			servo_flag.count_servo = 1 ; 
			servo_flag.servo_reset  = 1;
		}
	}
	//����״̬ - ���Ḵλ
	if(servo_flag.servo_reset == 1 )
	{
		
		if (servo_flag.count_servo == 1)
		{
			Reset_box_position();
			yyy = 0 ; 
			servo_flag.count_servo = 0;
                        //servo_flag.motor_back = 1 ; 
		}
		if(yyy > 0)
		{
			servo_flag.servo_reset = 0 ; 
			servo_flag.count_servo = 1 ; 
			
			servo_flag.motor_back = 1 ; 
			
		}
	}
	}
}


/**
 * @brief 3�ֺ����ȫ�����˶�ѧ����
 * 
 * @param target_speed m/s
 * @param target_dir rad
 * @param target_omega rad/s
 */

void Omni_ChassisSpeed_ctr_3Wheel(int s1,int s2,int s3)
{
    int16_t vel[3] = {s1,s2,s3};
     DJI_velCtrAll(vel);
    
}

float monitor = 0;




void Omni_ChassisMove_3Wheel(float speed,float direction,float target_angle)
{
    float absolute_angle_offset = 0; // ʹ�þ�������ʱ������ȫ����λ��õ�ƫ���ǽ��в���
    if (OmniChassis.base->pos_mode == POS_MODE_ABSOLUTE)
    {
        absolute_angle_offset = OmniChassis.base->PostureStatus.yaw - 1.5708; // ��ͷ��ʼ����90��
    }
    direction += absolute_angle_offset;
    
     //���ٶ�ͶӰ���������ӵ�ͶӰ��
    float speed_1 = -(speed * cos(InstallAngle_3Wheel[0] - direction));
    float speed_2 = -(speed * cos(InstallAngle_3Wheel[1] - direction));
    float speed_3 = (speed * cos(InstallAngle_3Wheel[2] - direction));
    //�����Ľ��ٶ�
    // float angle_speed = -PID_Release(&angle_pid, target_angle, chassis_tx.angle);//m/s   
    float angle_speed = target_angle * 0.1848;//target_angle * 0.1848; 
    
    float motor_1 = speed_1 - angle_speed;
    float motor_2 = speed_2 - angle_speed;
    float motor_3 = speed_3 - angle_speed;
    
    //��Ҫ���ٶ�m/sת��Ϊrpm
    float motor_rpm1 = (motor_1 / (WHEEL_RADIUS * 2 * 3.1415)) * 60 * 16;
    float motor_rpm2 = (motor_2 / (WHEEL_RADIUS * 2 * 3.1415)) * 60 * 16;
    float motor_rpm3 = (motor_3 / (WHEEL_RADIUS * 2 * 3.1415)) * 60 * 16;
    
    Omni_ChassisSpeed_ctr_3Wheel((int)(motor_rpm1),(int)(motor_rpm2),(int)(motor_rpm3));
}
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>������<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//


void Hmi_Exe()
{
	if(flag.track_left_flag == 1)
	{
		HMI_ShowCode_Left(&huart2);

	}
}



#endif

