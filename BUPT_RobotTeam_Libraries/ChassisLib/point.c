#include "point.h"

/*X    Y   SPEED   DIRECT  ANGLE*/

/*No.0 Bezier path has 41 points in total.*/
unsigned int points_pos1_num = 31;
unsigned int points_pos2_num = 31;
unsigned int points_pos3_num = 31;
unsigned int points_pos4_num = 31;
unsigned int points_pos5_num = 31;
unsigned int points_pos6_num = 31;
//������ߵĹ켣
Points points_pos1[] = {

{0.000000,     0.000000,     50,     0.650265,     1.570796},      /*point ranks 0*/

{0.136039,     0.103474,     74,     0.696831,     1.570796},      /*point ranks 1*/

{0.264069,     0.210621,     111,     0.745554,     1.570796},      /*point ranks 2*/

{0.384091,     0.321440,     146,     0.796340,     1.570796},      /*point ranks 3*/

{0.496104,     0.435931,     181,     0.849045,     1.570796},      /*point ranks 4*/

{0.600108,     0.554095,     216,     0.903472,     1.570796},      /*point ranks 5*/

{0.696104,     0.675932,     250,     0.959370,     1.570796},      /*point ranks 6*/

{0.784091,     0.801440,     284,     1.016437,     1.570796},      /*point ranks 7*/

{0.864069,     0.930621,     318,     1.074330,     1.570796},      /*point ranks 8*/

{0.936039,     1.063474,     354,     1.132672,     1.570796},      /*point ranks 9*/

{1.000000,     1.200000,     400,     1.190446,     1.570796},      /*point ranks 10*/

{1.088991,     1.422578,     400,     1.260087,     1.570796},      /*point ranks 11*/

{1.149641,     1.611455,     390,     1.343337,     1.570796},      /*point ranks 12*/

{1.187663,     1.775722,     364,     1.429850,     1.570796},      /*point ranks 13*/

{1.208768,     1.924468,     348,     1.501344,     1.570796},      /*point ranks 14*/

{1.218668,     2.066782,     345,     1.540410,     1.570796},      /*point ranks 15*/

{1.223075,     2.211754,     354,     1.541295,     1.570796},      /*point ranks 16*/

{1.227700,     2.368472,     373,     1.511421,     1.570796},      /*point ranks 17*/

{1.238254,     2.546026,     400,     1.464222,     1.570796},      /*point ranks 18*/

{1.260450,     2.753506,     400,     1.411704,     1.570796},      /*point ranks 19*/

{1.300000,     3.000000,     360,     1.384175,     1.570796},      /*point ranks 20*/

{1.326922,     3.142581,     277,     1.379377,     1.570796},      /*point ranks 21*/

{1.354528,     3.285033,     242,     1.374581,     1.570796},      /*point ranks 22*/

{1.382818,     3.427356,     208,     1.369784,     1.570796},      /*point ranks 23*/

{1.411792,     3.569550,     173,     1.364988,     1.570796},      /*point ranks 24*/

{1.441450,     3.711615,     138,     1.360193,     1.570796},      /*point ranks 25*/

{1.471792,     3.853550,     104,     1.355399,     1.570796},      /*point ranks 26*/

{1.502818,     3.995357,     69,     1.350606,     1.570796},      /*point ranks 27*/

{1.534528,     4.137033,     34,     1.345815,     1.570796},      /*point ranks 28*/

{1.566922,     4.278582,     30,     1.341026,     1.570796},      /*point ranks 29*/

{1.650000,     4.420000,     30,     1.341026,     1.570796},      /*point ranks 30*/






};
//��-����
/*No.1 Bezier path has 31 points in total.*/
Points points_pos2[] = {
  
{1.750000,     4.850000,     10,     -1.607261,     1.570796},      /*point ranks 0*/

{1.740981,     4.602778,     40,     -1.664252,     1.570796},      /*point ranks 1*/

{1.719522,     4.373828,     80,     -1.730335,     1.570796},      /*point ranks 2*/

{1.685623,     4.163150,     150,     -1.807137,     1.570796},      /*point ranks 3*/

{1.639283,     3.970743,     199,     -1.896339,     1.570796},      /*point ranks 4*/

{1.580503,     3.796607,     230,     -1.999404,     1.570796},      /*point ranks 5*/

{1.509283,     3.640743,     259,     -2.117098,     1.570796},      /*point ranks 6*/

{1.425623,     3.503150,     288,     -2.248816,     1.570796},      /*point ranks 7*/

{1.329522,     3.383828,     317,     -2.391920,     1.570796},      /*point ranks 8*/

{1.220981,     3.282779,     349,     -2.541532,     1.570796},      /*point ranks 9*/

{1.100000,     3.200000,     411,     -2.656912,     1.570796},      /*point ranks 10*/

{0.869758,     3.078760,     454,     -2.750157,     1.570796},      /*point ranks 11*/

{0.650301,     2.988183,     436,     -2.859901,     1.570796},      /*point ranks 12*/

{0.438438,     2.926873,     422,     -2.981789,     1.570796},      /*point ranks 13*/

{0.230978,     2.893435,     415,     -3.107855,     1.570796},      /*point ranks 14*/

{0.024731,     2.886474,     415,     3.054785,     1.570796},      /*point ranks 15*/

{-0.183495,     2.904595,     420,     2.948124,     1.570796},      /*point ranks 16*/

{-0.396890,     2.946403,     431,     2.860204,     1.570796},      /*point ranks 17*/

{-0.618645,     3.010504,     446,     2.792221,     1.570796},      /*point ranks 18*/

{-0.851952,     3.095501,     463,     2.742873,     1.570796},      /*point ranks 19*/

{-1.100000,     3.200000,     358,     2.644825,     1.570796},      /*point ranks 20*/

{-1.199110,     3.253729,     244,     2.484478,     1.570796},      /*point ranks 21*/

{-1.288418,     3.322629,     214,     2.328289,     1.570796},      /*point ranks 22*/

{-1.367924,     3.406701,     187,     2.183098,     1.570796},      /*point ranks 23*/

{-1.437627,     3.505944,     161,     2.053103,     1.570796},      /*point ranks 24*/

{-1.497528,     3.620358,     133,     1.939705,     1.570796},      /*point ranks 25*/

{-1.547627,     3.749944,     103,     1.842296,     1.570796},      /*point ranks 26*/

{-1.587924,     3.894701,     72,     1.759209,     1.570796},      /*point ranks 27*/

{-1.618418,     4.054629,     37,     1.688424,     1.570796},      /*point ranks 28*/

{-1.639110,     4.229729,     30,     1.627968,     1.570796},      /*point ranks 29*/

{-1.750000,     4.570000,     30,     1.627968,     1.570796},      /*point ranks 30*/


};
Points points_pos3[] = {
{-1.730000,     4.890000,     50,     -1.604815,     1.570796},      /*point ranks 0*/

{-1.736553,     4.697441,     79,     -1.582309,     1.570796},      /*point ranks 1*/

{-1.738761,     4.505672,     119,     -1.559607,     1.570796},      /*point ranks 2*/

{-1.736624,     4.314694,     159,     -1.536725,     1.570796},      /*point ranks 3*/

{-1.730142,     4.124508,     198,     -1.513690,     1.570796},      /*point ranks 4*/

{-1.719314,     3.935113,     238,     -1.490523,     1.570796},      /*point ranks 5*/

{-1.704142,     3.746508,     277,     -1.467247,     1.570796},      /*point ranks 6*/

{-1.684624,     3.558695,     316,     -1.443889,     1.570796},      /*point ranks 7*/

{-1.660761,     3.371672,     355,     -1.420472,     1.570796},      /*point ranks 8*/

{-1.632553,     3.185441,     395,     -1.397022,     1.570796},      /*point ranks 9*/

{-1.600000,     3.000000,     400,     -1.395714,     1.570796},      /*point ranks 10*/

{-1.550328,     2.719198,     400,     -1.412852,     1.570796},      /*point ranks 11*/

{-1.512580,     2.482194,     400,     -1.418430,     1.570796},      /*point ranks 12*/

{-1.481424,     2.279299,     400,     -1.404831,     1.570796},      /*point ranks 13*/

{-1.451528,     2.100821,     380,     -1.366258,     1.570796},      /*point ranks 14*/

{-1.417559,     1.937068,     371,     -1.304036,     1.570796},      /*point ranks 15*/

{-1.374186,     1.778350,     374,     -1.229063,     1.570796},      /*point ranks 16*/

{-1.316075,     1.614975,     390,     -1.156375,     1.570796},      /*point ranks 17*/

{-1.237896,     1.437253,     400,     -1.096499,     1.570796},      /*point ranks 18*/

{-1.134315,     1.235492,     400,     -1.052457,     1.570796},      /*point ranks 19*/

{-1.000000,     1.000000,     360,     -0.992216,     1.570796},      /*point ranks 20*/

{-0.936875,     0.903352,     247,     -0.904593,     1.570796},      /*point ranks 21*/

{-0.865556,     0.812626,     217,     -0.817585,     1.570796},      /*point ranks 22*/

{-0.786043,     0.727822,     187,     -0.732470,     1.570796},      /*point ranks 23*/

{-0.698334,     0.648939,     157,     -0.650365,     1.570796},      /*point ranks 24*/

{-0.602432,     0.575978,     127,     -0.572149,     1.570796},      /*point ranks 25*/

{-0.498334,     0.508939,     96,     -0.498435,     1.570796},      /*point ranks 26*/

{-0.386042,     0.447822,     65,     -0.429575,     1.570796},      /*point ranks 27*/

{-0.265556,     0.392626,     33,     -0.365692,     1.570796},      /*point ranks 28*/

{-0.136875,     0.343352,     30,     -0.306731,     1.570796},      /*point ranks 29*/

{0.000000,     0.300000,     30,     -0.306731,     1.570796},      /*point ranks 30*/


};


Points points_pos4[] = {
{0.000000,     0.000000,     50,     2.433792,     1.570796},      /*point ranks 0*/

{-0.103181,     0.088293,     66,     2.393942,     1.570796},      /*point ranks 1*/

{-0.201210,     0.179187,     99,     2.352879,     1.570796},      /*point ranks 2*/

{-0.294088,     0.272683,     132,     2.310689,     1.570796},      /*point ranks 3*/

{-0.381815,     0.368781,     164,     2.267483,     1.570796},      /*point ranks 4*/

{-0.464391,     0.467480,     195,     2.223388,     1.570796},      /*point ranks 5*/

{-0.541815,     0.568781,     227,     2.178555,     1.570796},      /*point ranks 6*/

{-0.614088,     0.672683,     259,     2.133147,     1.570796},      /*point ranks 7*/

{-0.681210,     0.779187,     290,     2.087345,     1.570796},      /*point ranks 8*/

{-0.743181,     0.888293,     322,     2.041337,     1.570796},      /*point ranks 9*/

{-0.800000,     1.000000,     371,     1.999780,     1.570796},      /*point ranks 10*/

{-0.886039,     1.188109,     396,     1.953464,     1.570796},      /*point ranks 11*/

{-0.949881,     1.346717,     362,     1.895605,     1.570796},      /*point ranks 12*/

{-0.996012,     1.483712,     336,     1.831677,     1.570796},      /*point ranks 13*/

{-1.028920,     1.606980,     320,     1.773814,     1.570796},      /*point ranks 14*/

{-1.053093,     1.724408,     316,     1.736051,     1.570796},      /*point ranks 15*/

{-1.073018,     1.843884,     323,     1.725383,     1.570796},      /*point ranks 16*/

{-1.093184,     1.973294,     341,     1.738291,     1.570796},      /*point ranks 17*/

{-1.118078,     2.120525,     368,     1.765530,     1.570796},      /*point ranks 18*/

{-1.152187,     2.293465,     400,     1.798288,     1.570796},      /*point ranks 19*/

{-1.200000,     2.500000,     360,     1.813060,     1.570796},      /*point ranks 20*/

{-1.247304,     2.691423,     320,     1.810384,     1.570796},      /*point ranks 21*/

{-1.294096,     2.882974,     280,     1.807706,     1.570796},      /*point ranks 22*/

{-1.340376,     3.074654,     240,     1.805030,     1.570796},      /*point ranks 23*/

{-1.386144,     3.266461,     200,     1.802354,     1.570796},      /*point ranks 24*/

{-1.431400,     3.458397,     160,     1.799678,     1.570796},      /*point ranks 25*/

{-1.476144,     3.650461,     120,     1.797002,     1.570796},      /*point ranks 26*/

{-1.520376,     3.842654,     80,     1.794326,     1.570796},      /*point ranks 27*/

{-1.564096,     4.034974,     40,     1.791651,     1.570796},      /*point ranks 28*/

{-1.607304,     4.227423,     30,     1.788976,     1.570796},      /*point ranks 29*/

{-1.682000,     4.420000,     30,     1.788976,     1.570796},      /*point ranks 30*/

};


Points points_pos5[] = {
   {-1.750000,     4.850000,     50,     -1.598902,     1.570796},      /*point ranks 0*/

{-1.757059,     4.598897,     80,     -1.541784,     1.570796},      /*point ranks 1*/

{-1.750327,     4.366929,     120,     -1.474668,     1.570796},      /*point ranks 2*/

{-1.729805,     4.154094,     160,     -1.395468,     1.570796},      /*point ranks 3*/

{-1.695491,     3.960393,     198,     -1.301904,     1.570796},      /*point ranks 4*/

{-1.647387,     3.785826,     228,     -1.191832,     1.570796},      /*point ranks 5*/

{-1.585491,     3.630393,     256,     -1.063896,     1.570796},      /*point ranks 6*/

{-1.509805,     3.494094,     283,     -0.918595,     1.570796},      /*point ranks 7*/

{-1.420327,     3.376929,     311,     -0.759388,     1.570796},      /*point ranks 8*/

{-1.317059,     3.278898,     342,     -0.593060,     1.570796},      /*point ranks 9*/

{-1.200000,     3.200000,     400,     -0.471631,     1.570796},      /*point ranks 10*/

{-0.949638,     3.072310,     400,     -0.383097,     1.570796},      /*point ranks 11*/

{-0.709665,     2.975599,     400,     -0.279281,     1.570796},      /*point ranks 12*/

{-0.477049,     2.908890,     400,     -0.163588,     1.570796},      /*point ranks 13*/

{-0.248757,     2.871208,     400,     -0.042409,     1.570796},      /*point ranks 14*/

{-0.021758,     2.861575,     400,     0.076103,     1.570796},      /*point ranks 15*/

{0.206983,     2.879017,     400,     0.184336,     1.570796},      /*point ranks 16*/

{0.440496,     2.922556,     400,     0.277200,     1.570796},      /*point ranks 17*/

{0.681815,     2.991217,     400,     0.352664,     1.570796},      /*point ranks 18*/

{0.933972,     3.084024,     400,     0.411113,     1.570796},      /*point ranks 19*/

{1.200000,     3.200000,     360,     0.517274,     1.570796},      /*point ranks 20*/

{1.306616,     3.260659,     254,     0.684326,     1.570796},      /*point ranks 21*/

{1.401761,     3.338282,     224,     0.846539,     1.570796},      /*point ranks 22*/

{1.485436,     3.432870,     196,     0.996341,     1.570796},      /*point ranks 23*/

{1.557642,     3.544423,     168,     1.129323,     1.570796},      /*point ranks 24*/

{1.618376,     3.672941,     140,     1.244285,     1.570796},      /*point ranks 25*/

{1.667642,     3.818424,     109,     1.342203,     1.570796},      /*point ranks 26*/

{1.705436,     3.980870,     76,     1.425108,     1.570796},      /*point ranks 27*/

{1.731761,     4.160282,     39,     1.495297,     1.570796},      /*point ranks 28*/

{1.746616,     4.356659,     30,     1.554934,     1.570796},      /*point ranks 29*/

{1.750000,     4.570000,     30,     1.554934,     1.570796},      /*point ranks 30*/

};


Points points_pos6[] = {
{1.730000,     4.890000,     50,     -1.536777,     1.570796},      /*point ranks 0*/

{1.736553,     4.697441,     79,     -1.559284,     1.570796},      /*point ranks 1*/

{1.738761,     4.505672,     119,     -1.581986,     1.570796},      /*point ranks 2*/

{1.736624,     4.314694,     159,     -1.604867,     1.570796},      /*point ranks 3*/

{1.730142,     4.124508,     198,     -1.627902,     1.570796},      /*point ranks 4*/

{1.719314,     3.935113,     238,     -1.651069,     1.570796},      /*point ranks 5*/

{1.704142,     3.746508,     277,     -1.674346,     1.570796},      /*point ranks 6*/

{1.684624,     3.558695,     316,     -1.697704,     1.570796},      /*point ranks 7*/

{1.660761,     3.371672,     355,     -1.721121,     1.570796},      /*point ranks 8*/

{1.632553,     3.185441,     395,     -1.744570,     1.570796},      /*point ranks 9*/

{1.600000,     3.000000,     400,     -1.745878,     1.570796},      /*point ranks 10*/

{1.550328,     2.719198,     400,     -1.728741,     1.570796},      /*point ranks 11*/

{1.512580,     2.482194,     400,     -1.723162,     1.570796},      /*point ranks 12*/

{1.481424,     2.279299,     400,     -1.736762,     1.570796},      /*point ranks 13*/

{1.451528,     2.100821,     380,     -1.775334,     1.570796},      /*point ranks 14*/

{1.417559,     1.937068,     371,     -1.837557,     1.570796},      /*point ranks 15*/

{1.374186,     1.778350,     374,     -1.912530,     1.570796},      /*point ranks 16*/

{1.316075,     1.614975,     390,     -1.985218,     1.570796},      /*point ranks 17*/

{1.237896,     1.437253,     400,     -2.045094,     1.570796},      /*point ranks 18*/

{1.134315,     1.235492,     400,     -2.089136,     1.570796},      /*point ranks 19*/

{1.000000,     1.000000,     360,     -2.149376,     1.570796},      /*point ranks 20*/

{0.936875,     0.903352,     247,     -2.237000,     1.570796},      /*point ranks 21*/

{0.865556,     0.812626,     217,     -2.324008,     1.570796},      /*point ranks 22*/

{0.786043,     0.727822,     187,     -2.409123,     1.570796},      /*point ranks 23*/

{0.698334,     0.648939,     157,     -2.491228,     1.570796},      /*point ranks 24*/

{0.602432,     0.575978,     127,     -2.569443,     1.570796},      /*point ranks 25*/

{0.498334,     0.508939,     96,     -2.643157,     1.570796},      /*point ranks 26*/

{0.386042,     0.447822,     65,     -2.712017,     1.570796},      /*point ranks 27*/

{0.265556,     0.392626,     33,     -2.775900,     1.570796},      /*point ranks 28*/

{0.136875,     0.343352,     30,     -2.834861,     1.570796},      /*point ranks 29*/

{0.000000,     0.300000,     30,     -2.834861,     1.570796},      /*point ranks 30*/

};

