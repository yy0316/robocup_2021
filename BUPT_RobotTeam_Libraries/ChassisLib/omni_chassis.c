/**
 * @file omni_chassis.c
 * @author simon
 * @brief 横辊子全向轮运动学相关
 * @version 0.1
 * @date 2021-04-25
 *
 * @copyright Copyright (c) 2021
 *
 */
#include "base_chassis.h"
#include "omni_chassis.h"
#include "chassis_common_config.h"
#include "can_utils.h"
#include "utils.h"
#include "handle.h"
#include <math.h>
#include "dji_board_can.h"
#include "cmd.h"
#include "move_test.h"
#include "main.h"
#include "uart_hmi.h"

#define USE_CHASSIS_OMNI
#define Limit(value, max) \
  if (value > max)        \
    value = max;          \
  else if (value < -max)  \
  value = -max
#ifdef USE_CHASSIS_OMNI
extern Flag flag;
extern float x_;
extern float y_;
OmniChassis_t OmniChassis;
static float InstallAngle[4] = {-2.3561945625, 2.3561945625, -0.7853981875, 0.7853981875}; // 车轮正方形与底盘坐标系x轴的夹角/rad
static CAN_Message_u MotorRpmCANMsg[4];
char MotorCANSendFlag = 0; // 需要在定时中断中置1，控制发送频率

/**
 * @brief 横辊子全向轮运动学解算
 *
 * @param target_speed m/s
 * @param target_dir rad
 * @param target_omega rad/s
 */
void Omni_ChassisMove(float target_speed, float target_dir, float target_omega)
{
  LIMIT(target_speed, DRIVE_WHEEL_MAX_SPEED);

  float absolute_angle_offset = 0; // 使用绝对坐标时，根据全场定位测得的偏航角进行补偿
  if (OmniChassis.base->pos_mode == POS_MODE_ABSOLUTE)
  {
    absolute_angle_offset = OmniChassis.base->PostureStatus.yaw - 1.5708; // 车头初始方向90°
  }
  target_dir -= absolute_angle_offset;

  // 将目标速度向量投影到每个驱动轮上
  float speed_out_0 = (target_speed * cos(InstallAngle[0] - target_dir));
  float speed_out_1 = (target_speed * cos(InstallAngle[1] - target_dir));
  float speed_out_2 = (target_speed * cos(InstallAngle[2] - target_dir));
  float speed_out_3 = (target_speed * cos(InstallAngle[3] - target_dir));

  // 与角速度在每个轮上的线速度合成
  float diagonal_length = Wheel2ChassisCenter;
  float motor_speed[4];
  motor_speed[0] = speed_out_0 + target_omega * diagonal_length;
  motor_speed[1] = speed_out_1 + target_omega * diagonal_length;
  motor_speed[2] = speed_out_2 + target_omega * diagonal_length;
  motor_speed[3] = speed_out_3 + target_omega * diagonal_length;

  if (OmniChassis.motor_can_flag)
  {
    for (int i = 0; i < 4; i++)
    {
      MotorRpmCANMsg[i].in[0] = 1;
      MotorRpmCANMsg[i].in[1] = (int)(motor_speed[i] / DRIVE_WHEEL_RADIUS * 30.0f / PI); // m/s转为rpm
      CAN_SendMsg(OmniChassis.drive_motors[i].can_id, &MotorRpmCANMsg[i]);
      HAL_Delay(1);
    }
    OmniChassis.motor_can_flag = 0;
  }
}

void Omni_Chassis3Move(float target_speed, float target_dir, float target_omega)
{

  Omni_ChassisMove_3Wheel(target_speed, target_dir, target_omega);
}

/**
 * @brief 初始化函数
 *
 */
void OmniChassis_Init()
{
  BaseChassis_Init();
  OmniChassis.base = &BaseChassis;
  OmniChassis.base->fChassisMove = Omni_Chassis3Move;
  //    for (int i = 0; i < 4; i++)
  //    {
  //        // VESC_Init(&OmniChassis.drive_motors[i],i + OMNI_DRIVE_MOTOR_BASE_CANID,(VESCMode)VESC_RPM); // 若使用本杰明驱动器
  //        MotorDriver_Init(&OmniChassis.drive_motors[i], i + OMNI_DRIVE_MOTOR_BASE_CANID, MTR_CTRL_RPM); // 若使用BUPT自制驱动卡
  //    }
  OmniChassis.motor_can_flag = 0;
}

/**
 * @brief 横辊子全向轮状态机
 *
 */

void OmniChassis_Exe()
{

  Chassis_UpdatePostureStatus();

  switch (OmniChassis.base->ctrl_mode)
  {
  case CTRL_MODE_NONE:
    break;
  case CTRL_MODE_HANDLE:
    Handle_Exe();

    break;
  case CTRL_MODE_CMD:
    OmniChassis.base->target_speed = CMD_TargetSpeed;
    OmniChassis.base->target_dir = CMD_TargetDir;
    OmniChassis.base->target_omega = CMD_TargetOmega;
    break;

  case CTRL_MODE_TRACK:
    OmniChassis.base->pos_mode = POS_MODE_ABSOLUTE;
    // test track left
    if (flag.track_left_flag == 1 && flag.first_flag == 0)
    {
      if (flag.first_intrack == 1)
      {
        OmniChassis.base->TrackStatus.finished = 0;
        flag.first_intrack = 0;
        OmniChassis.base->TrackStatus.track_path_index = 1;
        OmniChassis.base->TrackStatus.start = 1;
      }

      if (OmniChassis.base->TrackStatus.finished == 1)
      {
        if (flag.begin_count == 1)
        {
          xxx = 0;
          flag.begin_count = 0;
          servo_flag.servo_left = 1;
          servo_flag.motor_front = 1;
        }
        if (servo_flag.servo_finished == 0)
        {
          //Code_Scan();
          flag.left_voice = 1;
          
          Servo_Exe();
        }
        if (servo_flag.servo_finished == 1)
        {
         // HMI_PlayAudio_Left(&huart2);
//          zzz = 0;
//          for (;zzz < 500 ;){}
          //flag.left_voice = 0;
          //HMI_ShowCode_Left(&huart2);
          HMI_PlayAudio_Left(&huart2);
          zzz = 0;
          for (;zzz < 2000 ;){}
          flag.track_memory = 2;
          flag.track_cnt_flag++;
          flag.track_left_flag = 0;
          flag.first_flag = 1;
          flag.first_intrack = 1;
          flag.begin_count = 1;
          servo_flag.servo_finished = 0;
        }
      }
    }
    else if (flag.track_right_flag == 1 && flag.first_flag == 0)
    {
      //track right
      if (flag.first_intrack == 1)
      {
        OmniChassis.base->TrackStatus.finished = 0;
        flag.first_intrack = 0;
        OmniChassis.base->TrackStatus.track_path_index = 4;
        OmniChassis.base->TrackStatus.start = 1;
      }

      if (OmniChassis.base->TrackStatus.finished == 1)
      {
        if (flag.begin_count == 1)
        {
          xxx = 0;
          flag.begin_count = 0;
          servo_flag.servo_right = 1;
          servo_flag.motor_front = 1;
        }
        if (servo_flag.servo_finished == 0)
        {
          //Code_Scan();
          flag.right_voice = 1;
          //HMI_ShowCode_Right(&huart2);
          
          Servo_Exe();
        }
        if (servo_flag.servo_finished == 1)
        {
          //HMI_PlayAudio_Right(&huart2);
          HMI_PlayAudio_Right(&huart2);
          zzz = 0;
          for (;zzz < 2000 ;){}
          flag.right_voice = 0;
          flag.track_memory = 1;
          flag.track_cnt_flag++;
          flag.track_right_flag = 0;
          flag.first_flag = 1;
          flag.first_intrack = 1;
          flag.begin_count = 1;
          servo_flag.servo_finished = 0;
        }
      }
    }
    else if (flag.first_flag == 1 && flag.track_memory == 2 && flag.bed2bed_flag == 0)
    {
      //进行向右横移运动

      if (flag.first_intrack == 1)
      {
        OmniChassis.base->TrackStatus.finished = 0;
        flag.first_intrack = 0;

        OmniChassis.base->TrackStatus.track_path_index = 2;
        OmniChassis.base->TrackStatus.start = 1;
      }

      if (OmniChassis.base->TrackStatus.finished == 1)
      {
        if (flag.begin_count == 1)
        {
          xxx = 0;
          flag.begin_count = 0;
          servo_flag.servo_right = 1;
          servo_flag.motor_front = 1;
        }
        if (servo_flag.servo_finished == 0)
        {
          Code_Scan();
          flag.right_voice = 1;
          //HMI_ShowCode_Right(&huart2);
          
          Servo_Exe();
        }
        if (servo_flag.servo_finished == 1)
        {
          //HMI_PlayAudio_Right(&huart2);
          HMI_PlayAudio_Right(&huart2);
          zzz = 0;
          for (;zzz < 2000 ;){}
          uprintf("%d ms stop  \r\n", xxx);
         
          flag.track_memory = 2;
          flag.track_cnt_flag++;
          flag.first_intrack = 1;
          flag.begin_count = 1;
          flag.bed2bed_flag = 1;
          // servo_flag.servo_finished = 0;
        }
      }
    }
    else if (flag.first_flag == 1 && flag.track_memory == 1&&flag.bed2bed_flag == 0)
    {
      //进行向左横移运动
      if (flag.first_intrack == 1)
      {
        OmniChassis.base->TrackStatus.finished = 0;
        flag.first_intrack = 0;
        OmniChassis.base->TrackStatus.track_path_index = 5;
        OmniChassis.base->TrackStatus.start = 1;
      }

      if (OmniChassis.base->TrackStatus.finished == 1)
      {
        if (flag.begin_count == 1)
        {
          xxx = 0;
          flag.begin_count = 0;
          servo_flag.servo_left = 1;
          servo_flag.motor_front = 1;
          flag.left_voice = 1;
        }
        if (servo_flag.servo_finished == 0)
        {
        //  Code_Scan();
//flag.left_voice = 1;
         // HMI_ShowCode_Right(&huart2);

          
//          flag.left_voice = 0;
//          }
          
          Servo_Exe();
        }
        if (servo_flag.servo_finished == 1)
        {
          //HMI_PlayAudio_Left(&huart2);
          HMI_PlayAudio_Left(&huart2);
          
          zzz = 0;
          for (;zzz < 2000 ;){}
          uprintf("%d ms stop  \r\n", xxx);
         // flag.left_voice = 0;
          flag.track_memory = 1;
          flag.track_cnt_flag++;
          flag.first_intrack = 1;
          flag.begin_count = 1;
          flag.bed2bed_flag = 1;
         
          OmniChassis.base->TrackStatus.track_path_index = 7;
        }
      }
    }
    else if (flag.bed2bed_flag == 1 && flag.track_memory == 1)
    {
      //从左边床回到原点
      if (flag.first_intrack == 1)
      {
        Motor_back_real();
        OmniChassis.base->TrackStatus.finished = 0;
        flag.first_intrack = 0;
        OmniChassis.base->TrackStatus.track_path_index = 7;
        OmniChassis.base->TrackStatus.start = 1;
        flag.last_flag = 1;
      }

      if (OmniChassis.base->TrackStatus.finished == 1)
      {
        uprintf("mission success\r\n");
      }
    }
    else if (flag.bed2bed_flag == 1 && flag.track_memory == 2)
    {
      //从右边床回到原点
      if (flag.first_intrack == 1)
      {
        Motor_back_real();
        OmniChassis.base->TrackStatus.finished = 0;
        flag.first_intrack = 0;
        OmniChassis.base->TrackStatus.track_path_index = 3;
        OmniChassis.base->TrackStatus.start = 1;
        flag.last_flag = 1;
      }

      if (OmniChassis.base->TrackStatus.finished == 1)
      {
        uprintf("mission success\r\n");
      }
    }

    Chassis_TrackPathSets(OmniChassis.base->TrackStatus.track_path_index);
    break;
  case CTRL_MODE_TUNING:
    Chassis_YawTuning(CMD_TargetYaw);

    break;
  default:
    break;
  }

  // uprintf("speed:%.2f dir:%.2f\r\n",OmniChassis.base->target_speed,OmniChassis.base->target_dir);
  Omni_Chassis3Move(OmniChassis.base->target_speed, OmniChassis.base->target_dir, OmniChassis.base->target_omega); // 底层执行函数
}

// void OmniChassis_Exe_Test()
//{
//   Chassis_TrackPath(points_pos1,40);
// }
#endif
